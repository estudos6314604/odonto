# Projeto Odonto
Site para clínicas de odontologia
## Instruções de linha de comando do Git

Você também enviar arquivos existentes do seu computador usando as instruções abaixo.

### Configuração global do Git
```
git config --global user.name "Adriana Britto"
git config --global user.email "adriana.coelho.seg@gmail.com"
```

### Criar um novo repositório
```
git clone https://gitlab.com/estudos6314604/odonto.git
cd odonto
git switch --create main
touch README.md
git add README.md
git commit -m "add README"
git push --set-upstream origin main
```
### Faça push de uma pasta existente
```
cd existing_folder
git init --initial-branch=main
git remote add origin https://gitlab.com/estudos6314604/odonto.git
git add .
git commit -m "Initial commit"
git push --set-upstream origin main
```
### Faça push de um repositório Git existente
```
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/estudos6314604/odonto.git
git push --set-upstream origin --all
git push --set-upstream origin --tags
```

